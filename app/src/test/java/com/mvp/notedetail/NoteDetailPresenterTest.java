package com.mvp.notedetail;

import com.mvp.data.DataManager;
import com.mvp.ui.notedetail.NoteDetailMvpView;
import com.mvp.ui.notedetail.NoteDetailPresenter;
import com.mvp.utils.RxSchedulersOverrideRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.Mock;

/**
 * Created by hoannt on 7/23/16.
 */
public class NoteDetailPresenterTest  {
    @Mock
    NoteDetailMvpView mNoteDetailMvpView;

    @Mock
    DataManager mDataManager;


    @Rule
    RxSchedulersOverrideRule mRxSchedulersOverrideRule = new RxSchedulersOverrideRule();

    private NoteDetailPresenter mNoteDetailPresenter ;

    @Before
    public void setUp(){
        mNoteDetailPresenter = new NoteDetailPresenter(mDataManager);
        mNoteDetailPresenter.attachView(mNoteDetailMvpView);
    }

    @After
    public void detachView(){
        mNoteDetailPresenter.detachView();
    }
}
