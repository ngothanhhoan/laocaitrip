package com.mvp.addNote;

import com.mvp.data.DataManager;
import com.mvp.data.model.Note;
import com.mvp.ui.addnote.AddNoteMvpView;
import com.mvp.ui.addnote.AddNotePresenter;
import com.mvp.utils.RxSchedulersOverrideRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import rx.Observable;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

/**
 * Created by hoannt on 7/21/16.
 */

@RunWith(MockitoJUnitRunner.class)
public class AddNotePresenterTest {

    @Mock
    AddNoteMvpView mAddNoteMvpView;

    @Mock
    DataManager mDataManager;


    @Rule
    public RxSchedulersOverrideRule rxSchedulersOverrideRule = new RxSchedulersOverrideRule();

    private AddNotePresenter mAddNotePresenter;

    @Before
    public void setUp(){
        mAddNotePresenter = new AddNotePresenter(mDataManager);
        mAddNotePresenter.attachView(mAddNoteMvpView);

    }

    @After
    public void detachView(){
        mAddNotePresenter.detachView();
    }

    @Test
    public void saveNoteToDb_showSuccessfulUI(){

        //when presenter is asked for save a note
       Note note = new Note("test","test");
        doReturn(Observable.just((Long)1L)).when(mDataManager).saveNoteToDb(note);
        mAddNotePresenter.saveNotesObject(note);
        //then a note is
        verify(mDataManager).saveNoteToDb(any(Note.class));
        verify(mAddNoteMvpView).showNotesList();
    }

    @Test
    public void emptyNote_showErrorUi(){
        mAddNotePresenter.saveNote("", "");

        // Then an empty not error is shown in the UI
        verify(mAddNoteMvpView).showEmptyNoteError();
    }




}
