package com.mvp.notes;

import com.mvp.MockModelFabric;
import com.mvp.data.DataManager;
import com.mvp.data.model.Note;
import com.mvp.ui.notes.NotesMvpView;
import com.mvp.ui.notes.NotesPresenter;
import com.mvp.utils.RxSchedulersOverrideRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import rx.Observable;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

/**
 * Created by hoannt on 7/19/16.
 */

@RunWith(MockitoJUnitRunner.class)
public class NotePresenterTest {

    @Mock
    NotesMvpView mNotesMvpView;
    @Mock
    DataManager mDataManager;

    @Rule
    public RxSchedulersOverrideRule mRxSchedulersOverrideRule = new RxSchedulersOverrideRule();

    private NotesPresenter mNotesPresenter;

    @Before
    public void setUp() {
        mNotesPresenter = new NotesPresenter(mDataManager);
        mNotesPresenter.attachView(mNotesMvpView);
    }

    @After
    public void detachView() {
        mNotesPresenter.detachView();
    }


    @Test
    public void loadNoteSuccessful(){
        List<Note> list = MockModelFabric.newNoteList(20);
        stubDataManagerGetNotes(Observable.just(list));
        mNotesPresenter.loadNotes();
        verify(mNotesMvpView).showNotes(list);
    }

    @Test
    public void clickOnFab_showAddNoteUi(){
        //when adding new note
        mNotesPresenter.addNewNote();

        //then addnote ui show
        verify(mNotesMvpView).showAddNote();
    }

    @Test

    public void clickOnNote_showDetailUi(){
        Note note = MockModelFabric.newNote();
        //open note

        mNotesPresenter.openNoteDetail(note);
        //THen note detail is shown

        verify(mNotesMvpView).showNoteDetailUi(any(String.class));
    }

    private void stubDataManagerGetNotes(Observable observable) {
        doReturn(observable)
                .when(mDataManager).loadNotes();
    }

}
