package com.mvp.ui.notes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mvp.R;
import com.mvp.data.model.Note;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hoannt on 7/19/16.
 */
public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {

    private List<Note> mNotes;
    private Callback mCallback;

    @Inject
    public NotesAdapter() {
        mNotes = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View noteView = inflater.inflate(R.layout.item_note, parent, false);

        return new ViewHolder(noteView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Note note = mNotes.get(position);
        viewHolder.setNote(note);
        viewHolder.title.setText(note.getTitle());
        viewHolder.description.setText(note.getDescription());
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public void replaceData(List<Note> notes) {
        setList(notes);
        notifyDataSetChanged();
    }

    private void setList(List<Note> notes) {
        if (notes !=null){
        mNotes = notes;
        }
    }

    @Override
    public int getItemCount() {
        return mNotes.size();
    }

    public Note getItem(int position) {
        return mNotes.get(position);
    }




    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.note_detail_title) TextView title;

        @BindView(R.id.note_detail_description) TextView description;

        public Note note;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.layout_item)
        void onItemClicked() {
            if (mCallback != null) mCallback.onNoteClicked(note);
        }

        public void setNote(Note note){
            this.note = note;
        }
    }

    interface Callback {
        void onNoteClicked(Note note);
    }
}
