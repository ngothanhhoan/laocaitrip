package com.mvp.ui.notes;

import com.mvp.data.DataManager;
import com.mvp.data.model.Note;
import com.mvp.ui.base.Presenter;

import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by hoannt on 5/25/16.
 */
public class NotesPresenter implements Presenter<NotesMvpView> {

    private NotesMvpView mNotesMvpView;
    private DataManager mDataManager;
    public Subscription mSubscription;
    @Inject
    public NotesPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(NotesMvpView mvpView) {
        mNotesMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mNotesMvpView = null;
    }

    public void addNewNote(){
        mNotesMvpView.showAddNote();
    }

    public void loadNotes(){
        mSubscription =  mDataManager.loadNotes()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<Note>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(List<Note> notes) {

                    if(!notes.isEmpty()){
                        mNotesMvpView.showNotes(notes);
                    }
                    }
                });
    }

    public void openNoteDetail(Note note) {
        mNotesMvpView.showNoteDetailUi(note.getId());
    }
}
