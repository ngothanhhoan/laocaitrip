package com.mvp.ui.notes;

import com.mvp.data.model.Note;
import com.mvp.ui.base.MvpView;

import java.util.List;

/**
 * Created by hoannt on 5/25/16.
 */
public interface NotesMvpView extends MvpView {
   void setProgressIndicator(boolean active);

   void showNotes(List<Note> notes);

   void showAddNote();

   void showNoteDetailUi(String noteId);
}
