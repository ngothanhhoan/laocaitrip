package com.mvp.ui.notedetail;

import android.util.Log;

import com.mvp.data.DataManager;
import com.mvp.data.model.Note;
import com.mvp.ui.base.Presenter;

import javax.inject.Inject;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by hoannt on 7/20/16.
 */
public class NoteDetailPresenter implements Presenter<NoteDetailMvpView> {


    private DataManager mDataManager;
    private NoteDetailMvpView mNoteDetailMvpView;
    private Subscription subscription;
    @Inject
    public NoteDetailPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(NoteDetailMvpView mvpView) {
        mNoteDetailMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mNoteDetailMvpView = null;
        subscription.unsubscribe();
    }

    public void openNote(String id){
      subscription = mDataManager.openNote(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Note>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Note",e.getMessage().toString());
                    }

                    @Override
                    public void onNext(Note note) {
                        mNoteDetailMvpView.showTitle(note.getTitle());
                        mNoteDetailMvpView.showDescription(note.getDescription());
                    }
                });
    }
}
