package com.mvp.ui.notedetail;

import com.mvp.ui.base.MvpView;

/**
 * Created by hoannt on 7/20/16.
 */
public interface NoteDetailMvpView extends MvpView {

    void setProgressIndicator(boolean active);

    void showMissingNote();

    void hideTitle();

    void showTitle(String title);

    void showImage(String imageUrl);

    void hideImage();

    void hideDescription();

    void showDescription(String description);
}
