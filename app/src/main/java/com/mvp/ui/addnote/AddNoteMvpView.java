package com.mvp.ui.addnote;

import com.mvp.ui.base.MvpView;

/**
 * Created by hoannt on 7/19/16.
 */
public interface AddNoteMvpView extends MvpView {

    void showEmptyNoteError();

    void showNotesList();


//    void openCamera(String saveTo);

//    void showImagePreview(@NonNull String uri);
//
//    void showImageError();

}
