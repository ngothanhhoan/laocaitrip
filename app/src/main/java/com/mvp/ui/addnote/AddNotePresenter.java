/*
 * Copyright 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mvp.ui.addnote;


import com.mvp.data.DataManager;
import com.mvp.data.model.Note;
import com.mvp.ui.base.Presenter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Listens to user actions from the UI ({@link AddNoteFragment}), retrieves the data and updates
 * the UI as required.
 */
public class AddNotePresenter implements Presenter<AddNoteMvpView> {

    private AddNoteMvpView mAddNoteMvpView;

    private DataManager mDataManager;

    public Subscription mSubscription;

    @Inject
    public AddNotePresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(AddNoteMvpView mvpView) {
        mAddNoteMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mAddNoteMvpView = null;
    }

    public void saveNote(String title, String description) {
        if(title.isEmpty() && description.isEmpty()){
            mAddNoteMvpView.showEmptyNoteError();
            return;
        }
        Note note = new Note(title, description);
        saveNotesObject(note);
    }

    public void saveNotesObject(Note note){
        mSubscription = mDataManager.saveNoteToDb(note)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(Long aLong) {
                        mAddNoteMvpView.showNotesList();
                    }
                });

    }

    public void takePicture() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
//        mImageFile.create(imageFileName, ".jpg");
//        mAddNoteMvpView.openCamera(mImageFile.getPath());
    }
}
