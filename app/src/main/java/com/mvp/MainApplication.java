package com.mvp;

import android.app.Application;
import android.content.Context;
import com.mvp.injection.component.ApplicationComponent;
import com.mvp.injection.component.DaggerApplicationComponent;
import com.mvp.injection.module.ApplicationModule;

/**
 * Created by hoannt on 5/24/16.
 */
public class MainApplication extends Application {

    ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(new ApplicationModule(this))
            .build();
        mApplicationComponent.inject(this);
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

    public static MainApplication get(Context context) {
        return (MainApplication) context.getApplicationContext();
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}
