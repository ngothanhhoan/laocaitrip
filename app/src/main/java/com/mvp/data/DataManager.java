package com.mvp.data;

import com.mvp.data.local.DatabaseHelper;
import com.mvp.data.model.Note;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;


@Singleton
public class DataManager {

    private final DatabaseHelper mDatabaseHelper;

    @Inject
    public DataManager(DatabaseHelper databaseHelper) {
        mDatabaseHelper = databaseHelper;
    }


    public Observable<Long> saveNoteToDb(Note note){
        return mDatabaseHelper.saveNote(note);
    }


    public Observable<List<Note>> loadNotes() {
       return mDatabaseHelper.loadNotes();

    }

    public Observable<Note> openNote(String id) {
         return mDatabaseHelper.openNote(id);
    }
}
