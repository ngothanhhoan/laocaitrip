package com.mvp.data.local;

import android.database.Cursor;
import android.util.Log;

import com.mvp.data.model.Note;
import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.Subscriber;

@Singleton
public class DatabaseHelper {

    private final BriteDatabase mDb;

    @Inject
    public DatabaseHelper(DbOpenHelper dbOpenHelper) {
        mDb = SqlBrite.create().wrapDatabaseHelper(dbOpenHelper);
    }

    public BriteDatabase getBriteDb() {
        return mDb;
    }

    /**
     * Remove all the data from all the tables in the database.
     */
    public Observable<Void> clearTables() {
        return Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(Subscriber<? super Void> subscriber) {
                BriteDatabase.Transaction transaction = mDb.newTransaction();
                try {
                    Cursor cursor = mDb.query("SELECT name FROM sqlite_master WHERE type='table'");
                    while (cursor.moveToNext()) {
                        mDb.delete(cursor.getString(cursor.getColumnIndex("name")), null);
                    }
                    cursor.close();
                    transaction.markSuccessful();
                    subscriber.onCompleted();
                } finally {
                    transaction.end();
                }
            }
        });
    }


    /**
     * Insert note to database
     */

    public Observable<Long> saveNote(final Note note) {
        Log.d("Note", "save note");
       return Observable.create(new Observable.OnSubscribe<Long>() {
            @Override
            public void call(Subscriber<? super Long> subscriber) {
              long index =  mDb.insert(Db.NoteTable.TABLE_NAME, Db.NoteTable.toContentValues(note));
                subscriber.onNext(index);
                subscriber.onCompleted();
            }
        });
    }

    public Observable<List<Note>> loadNotes() {
        List<Note> list = new ArrayList<Note>();
        Cursor cursor = mDb.query("SELECT * FROM " + Db.NoteTable.TABLE_NAME);
        while (cursor.moveToNext()) {
            list.add(Db.NoteTable.parseCursor(cursor));
        }
        cursor.close();
        return Observable.just(list);
    }


    public Observable<Note> openNote(final String id) {
      return Observable.create(new Observable.OnSubscribe<Note>() {
          @Override
          public void call(Subscriber<? super Note> subscriber) {
              Cursor cursor = mDb.query("SELECT * FROM "+
                      Db.NoteTable.TABLE_NAME + " WHERE " +
                      Db.NoteTable.COLUMN_ID + "=?", id);
              while (cursor.moveToNext()){
                  String title = cursor.getString(cursor.getColumnIndexOrThrow(Db.NoteTable.COLUMN_TITLE));
                  String description = cursor.getString(cursor.getColumnIndexOrThrow(Db.NoteTable.COLUMN_DESCRIPTION));
                  Note note = new Note(title,description);
                  subscriber.onNext(note);
              }
              cursor.close();
              subscriber.onCompleted();
          }
      });
    }
}
