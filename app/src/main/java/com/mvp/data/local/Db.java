package com.mvp.data.local;


import android.content.ContentValues;
import android.database.Cursor;

import com.mvp.data.model.Note;

public class Db {

    public Db() {
    }

    public static final class NoteTable {
        public static final String TABLE_NAME = "note";

        public static final String COLUMN_ID = "mId";
        public static final String COLUMN_TITLE = "mTitle";
        public static final String COLUMN_DESCRIPTION = "mDescription";
        public static final String COLUMN_IMAGE = "mImageUrl";

        public static final String CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_ID + " TEXT PRIMARY KEY," +
                        COLUMN_TITLE + " TEXT NOT NULL," +
                        COLUMN_DESCRIPTION + " TEXT NOT NULL," +
                        COLUMN_IMAGE + " TEXT" +
                        " );";

        public static ContentValues toContentValues(Note note) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_ID, note.getId());
            values.put(COLUMN_TITLE, note.getTitle());
            values.put(COLUMN_DESCRIPTION, note.getDescription());
            values.put(COLUMN_IMAGE, note.getImageUrl());
            return values;
        }

        public static Note parseCursor(Cursor cursor) {
            String id  = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ID));
            String title = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TITLE));
            String description = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DESCRIPTION));
            String image = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_IMAGE));
            Note note =new  Note(title, description, image);
            note.setId(id);
            return note;
        }
    }

}
