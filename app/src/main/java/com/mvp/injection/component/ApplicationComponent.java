package com.mvp.injection.component;

import android.content.Context;

import com.mvp.MainApplication;
import com.mvp.data.DataManager;
import com.mvp.data.local.DatabaseHelper;
import com.mvp.injection.ApplicationContext;
import com.mvp.injection.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MainApplication ribotApplication);

    @ApplicationContext
    Context context();

    DatabaseHelper databaseHelper();

    DataManager dataManager();
}
