package com.mvp.injection.component;

import com.mvp.injection.PerActivity;
import com.mvp.injection.module.ActivityModule;
import com.mvp.ui.addnote.AddNoteActivity;
import com.mvp.ui.addnote.AddNoteFragment;
import com.mvp.ui.notedetail.NoteDetailActivity;
import com.mvp.ui.notedetail.NoteDetailFragment;
import com.mvp.ui.notes.NotesActivity;
import com.mvp.ui.notes.NotesFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject (NotesActivity mMainActivity);
    void inject (AddNoteActivity mAddNoteActivity);
    void inject (NoteDetailActivity mNoteDetailActivity);

    void inject (NotesFragment mNotesFragment);
    void inject (AddNoteFragment mAddNoteFragment);
    void inject (NoteDetailFragment mNoteDetailFragment);
}

