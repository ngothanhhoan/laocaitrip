package com.mvp;

import com.mvp.data.model.Note;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MockModel {

    public static String randomString() {
        return UUID.randomUUID().toString();
    }


   public static Note newNote(){
   return new Note(randomString(),randomString());
   }

    public static List<Note> newNoteList(int size){
        ArrayList<Note> list = new ArrayList<>();
        for(int i=0; i < size ; i++){
            list.add(newNote());
        }
        return list;
    }

}
