package com.mvp.notescreen;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;

import com.mvp.MockModel;
import com.mvp.R;
import com.mvp.TestComponentRule;
import com.mvp.data.model.Note;
import com.mvp.ui.notes.NotesActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;

import java.util.List;

import rx.Observable;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
/**
 * Created by hoannt on 7/28/16.
 */
public class NotesActivityTest  {
    public final TestComponentRule component =
            new TestComponentRule(InstrumentationRegistry.getTargetContext());

    public final IntentsTestRule<NotesActivity> main =
            new IntentsTestRule<>(NotesActivity.class, false, false);

    // TestComponentRule needs to go first so we make sure the ApplicationTestComponent is set
    // in the Application before any Activity is launched.
    @Rule
    public TestRule chain = RuleChain.outerRule(component).around(main);

    private List<Note> mList;

    @Before
    public void setUp(){
        mList = MockModel.newNoteList(1);
        doReturn(Observable.just(mList))
                .when(component.getMockDataManager())
                .loadNotes();
    }


    @Test
    public void clickAddNoteButton_opensAddNoteUi() throws Exception {

        main.launchActivity(null);
        // Click on the add note button
        onView(ViewMatchers.withId(R.id.fab_add_notes)).perform(click());

        // Check if the add note screen is displayed
        onView(withId(R.id.add_note_title)).check(matches(isDisplayed()));
    }

    @Test
    public void addNoteToNotesList() throws Exception {
        String newNoteTitle = "Espresso";
        String newNoteDescription = "UI testing for Android";
        Note note = new Note(newNoteTitle,newNoteDescription);

        main.launchActivity(null);


        // Click on the add note button
        onView(withId(R.id.fab_add_notes)).perform(click());

        // Add note title and description
        // Type new note title
        onView(withId(R.id.add_note_title)).perform(typeText(newNoteTitle), closeSoftKeyboard());
        onView(withId(R.id.add_note_description)).perform(typeText(newNoteDescription),
                closeSoftKeyboard()); // Type new note description and close the keyboard

        doReturn(Observable.just(2L))
                .when(component.getMockDataManager())
                .saveNoteToDb(any(Note.class));
        mList.add(note);
        // Save the note
        onView(withId(R.id.fab_add_notes)).perform(click());
        // Scroll notes list to added note, by finding its description
        onView(withId(R.id.notes_list)).perform(
                scrollTo(hasDescendant(withText(newNoteDescription))));

        // Verify note is displayed on screen
        onView(withText(newNoteDescription)).check(matches(isDisplayed()));
    }


}
